import {initializeApp} from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'

export const appName = 'psychopathy-online'

export const firebaseConfig = {
    apiKey: "AIzaSyBkTZasW61jimeZ4lmgSwCVcJlh1ifJjRQ",
    authDomain: `${appName}.firebaseapp.com`,
    databaseURL: `https://${appName}.firebaseio.com`,
    projectId: appName,
    storageBucket: `${appName}.appspot.com`,
    messagingSenderId: "927036880535"
};

initializeApp(firebaseConfig)
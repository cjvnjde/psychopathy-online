import { Record } from 'immutable'
import firebase from 'firebase/app'
import { takeEvery, put, call, all } from 'redux-saga/effects'
import { appName } from '../config'


/**
 * Constants
 * */
export const moduleName = 'quiz'
const prefix = `${appName}/${moduleName}`

export const RANDOM_DATA_REQUEST = `${prefix}/SIGN_IN_REQUEST`
export const RANDOM_DATA_SUCCESS = `${prefix}/SIGN_IN_SUCCESS`

/**
 * Reducer
 * */
export const ReducerRecord = Record({
    id: null,
    users: null,
    name: null,
    questions: null,
    description: null,
    questionType: null,
    persons: null,
    personGroups: null,
    schemas: null
})

export default function reducer(state = new ReducerRecord(), action) {
    const { type } = action

    switch (type) {

        default:
            return state
    }
}

/**
 * Selectors
 * */

/**
 * Action Creators
 * */
export function addRandomData(){
    return {
        type: RANDOM_DATA_REQUEST
    }
}

/**
 * Sagas
 */

export function* randomDataSaga() {

    let db = firebase.firestore()

    db.settings({timestampsInSnapshots: true})

    db = db.collection("users")

    yield call([db, db.add], {
        first: "Ada",
        last: "Lovelace",
        born: 18666324
    })

    yield put({
        type: RANDOM_DATA_SUCCESS
    })

}

export function* saga() {
    yield all([
        takeEvery(RANDOM_DATA_REQUEST, randomDataSaga),
    ])
}
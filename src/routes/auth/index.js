import React, {Component} from 'react'
import {Route, Redirect} from 'react-router-dom'
import SignInForm from '../../components/auth/sign-in'
import SignUpForm from '../../components/auth/sign-up'
import { signIn, signUp, isAuthorizedSelector } from '../../ducks/auth'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'

import './style.css'

class AuthRoute extends Component {
    static propTypes = {
        //from redux
        isAuthorized: PropTypes.bool,
        signIn: PropTypes.func,
        signUp: PropTypes.func

    }
    render() {
        if(this.props.isAuthorized) return <Redirect to="/admin"/>
        return (
            <div className='auth'>
                <Route path='/auth/sign-in' render={this.signInForm}/>
                <Route path='/auth/sign-up' render={this.signUpForm}/>
            </div>
        )
    }

    signInForm = () => (
        <SignInForm onSubmit={this.handleSignIn}/>
    )

    signUpForm = () => (
        <SignUpForm onSubmit={this.handleSignUp}/>
    )

    handleSignIn = ({ email, password }) => this.props.signIn(email, password)
    handleSignUp = ({ email, password }) => this.props.signUp(email, password)
}

export default connect(
    (state) => ({
        isAuthorized: isAuthorizedSelector(state)
    }),
    {signIn, signUp}
)(AuthRoute)
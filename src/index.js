import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {ConnectedRouter} from "connected-react-router"
import Provider from "react-redux/es/components/Provider"
import store from './redux'
import history from './history'
import registerServiceWorker from './registerServiceWorker'

import './style.css'

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
    , document.getElementById('root'))
registerServiceWorker()

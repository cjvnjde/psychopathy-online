import {combineReducers} from 'redux'
import {reducer as form} from 'redux-form'
import authReducer, { moduleName as authModule } from '../ducks/auth'
import quizReducer, { moduleName as quizModule } from '../ducks/quiz'

export default combineReducers({
    form,
    [authModule]: authReducer,
    [quizModule]: quizReducer
})
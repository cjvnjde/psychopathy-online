import {createStore, applyMiddleware} from 'redux'
import reducer from './reducer'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import {connectRouter, routerMiddleware} from 'connected-react-router'
import history from '../history'
import saga from './saga'

const sagaMiddleware = createSagaMiddleware()

const enhancer = applyMiddleware(
    sagaMiddleware,
    routerMiddleware(history),
    logger)

const store = createStore(connectRouter(history)(reducer), enhancer)

sagaMiddleware.run(saga)

//dev only
window.store = store

export default store
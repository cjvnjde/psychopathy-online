import { all } from 'redux-saga/effects'
import { saga as authSaga } from '../ducks/auth'
import { saga as quizSaga } from '../ducks/quiz'

export default function*() {
    yield all([
        authSaga(),
        quizSaga()
    ])
}
import React, {Component} from 'react'
import {reduxForm, Field, Form} from 'redux-form'
import ErrorField from '../../common/error-field/index'
import { validate as validateEmail } from 'email-validator'
import PropTypes from "prop-types"
import {Link} from "react-router-dom"

import './style.css'

class SignUpForm extends Component {
    static propTypes = {
        //from reduxForm
        handleSubmit: PropTypes.func
    }

    render() {
        return (
                <div className='sign-up'>
                    <Form onSubmit={this.props.handleSubmit}>
                        <Field
                            label='Email'
                            name='email'
                            component={ErrorField} />
                        <Field
                            label='Пароль'
                            name='password'
                            component={ErrorField}
                            type='password' />
                        <Field
                            label='Пароль'
                            name='password_repeat'
                            component={ErrorField}
                            type='password' />
                        <div className='sign-up__button__container'>
                            <button type='submit' className='sign-up__registration'>Регистрация</button>
                            <Link to='/auth/sign-in' className='sign-up__enter'>Вход</Link>
                        </div>
                    </Form>
                </div>


        )
    }
}

const validate = ({ email, password, password_repeat }) => {
    const errors = {}

    if (!email) errors.email = 'Email обязателен'
    else if (!validateEmail(email)) errors.email = 'Некорректный email'

    if (!password) errors.password = 'Пароль обязателен'
    else if (password.length < 8) errors.password = 'Короткий пароль'
    else if (password_repeat !== password) errors.password = 'Пароли не совпадают'

    return errors
}

export default reduxForm({
    form: 'auth',
    validate
})(SignUpForm)
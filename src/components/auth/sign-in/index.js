import React, {Component} from 'react'
import {reduxForm, Field, Form} from 'redux-form'
import ErrorField from '../../common/error-field'
import { validate as validateEmail } from 'email-validator'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

import './style.css'

class SignInForm extends Component {
    static propTypes = {
        //from reduxForm
        handleSubmit: PropTypes.func
    }
    render() {
        return (
            <div className='sign-in'>
                <Form onSubmit={this.props.handleSubmit}>
                    <Field
                        label='Email'
                        name='email'
                        component={ErrorField} />
                    <Field
                        label='Пароль'
                        name='password'
                        component={ErrorField}
                        type='password' />

                    <div className='sign-in__button__container'>
                        <Link to='/auth/sign-up' className='sign-in__registration'>Регистрация</Link>
                        <button type='submit' className='sign-in__enter'>Вход</button>
                    </div>
                </Form>
            </div>
        )
    }
}

const validate = ({email, password}) => {
    const errors = {}

    if (!email) errors.email = 'Email обязателен'
    else if (!validateEmail(email)) errors.email = 'Некорректный email'

    if (!password) errors.password = 'Пароль обязателен'

    return errors
}



export default reduxForm({
    form: 'auth',
    validate
})(SignInForm)
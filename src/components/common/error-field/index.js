import React, {Fragment} from 'react'

import './style.css'

const Index = ({label, type, input, meta: {error, touched } }) => {
    const errorText = touched  &&
        error && <Fragment>{error}</Fragment>
    return (
        <div className='error-field'>
            <div className='error-field__text__container'>
                <div className='error-field__label'>
                    {label}
                </div>
                <div className='error-field__errorText'>
                    {errorText}
                </div>
            </div>
            <div className='error-field__input__container'>
                <input {...input} type={type} />
            </div>
        </div>
    )
}

export default Index
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {isAuthorizedSelector, signOut} from "../../ducks/auth"
import {Link} from 'react-router-dom'

import './style.css'
import user_logo from '../../images/user_logo.svg'

class Header extends Component {
    static propTypes = {
        //from redux
        isAuthorized: PropTypes.bool,
        signOut: PropTypes.func
    }

    render() {
        return (
            <div className='header'>
                <Link className='header__toMain' to='/admin'>На главную</Link>
                {this.getUserField()}
                {/*{this.props.isAuthorized?
                    <button onClick={this.props.signOut}>Выход</button>
                    :<NavLink to='/auth/sign-in'>Вход</NavLink>}*/}
            </div>
        )
    }

    getUserField = () => {
        const {isAuthorized} = this.props

        if(!isAuthorized) return (
            <div className='header__user__container'>
                <Link to='/auth/sign-in'>Вход</Link>
                {' / '}
                <Link to='/auth/sign-up'>Регистрация</Link>
            </div>
        )

        return (
            <div className='header__user__container'>
                <img src={user_logo} className="header__user-logo" alt="logo" />
                <a onClick={this.props.signOut}>Выход</a>
            </div>
        )

    }
}


export default connect((state) => ({
    isAuthorized: isAuthorizedSelector(state)
}), {signOut})(Header)
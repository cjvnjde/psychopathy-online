import React, {Component} from 'react'
import {userSelector} from "../ducks/auth"
import {connect} from 'react-redux'
import {addRandomData} from '../ducks/quiz'

class HelloWorld extends Component {
    render() {
        return (
            <div>
                Hello world! {this.props.user}
                <button onClick={this.handleClick}>Random</button>
            </div>
        )
    }

    handleClick = () => {
        this.props.addRandomData()
    }
}

export default connect((state) => ({
    user: userSelector(state).uid
}), {addRandomData})(HelloWorld)
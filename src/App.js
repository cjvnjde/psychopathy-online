import React, {Component} from 'react'
import Auth from './routes/auth'
import {Route} from 'react-router-dom'
import HelloWorld from './components/hello-world'
import ProtectedRoute from './components/common/protected-route'
import './config'
import Header from "./components/header"

class App extends Component {
  render() {
    return (
        <div>
            <Header/>
            <Route path="/auth" component={Auth} />
            <ProtectedRoute path="/admin" component={HelloWorld} />
        </div>

    )
  }
}

export default App
